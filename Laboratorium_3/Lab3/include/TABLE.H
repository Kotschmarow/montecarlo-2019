#ifndef TABLE_H
#define TABLE_H


class Table
{
    public:
        Table();
        Table(int size);
        virtual ~Table();

    protected:
    *tab;
    int size;
    private:

};

#endif // TABLE_H
